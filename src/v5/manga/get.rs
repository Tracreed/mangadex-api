//! Builder for the manga view endpoint.
//!
//! <https://api.mangadex.org/docs.html#operation/get-manga-id>
//!
//! # Examples
//!
//! ```rust
//! use uuid::Uuid;
//!
//! use mangadex_api::v5::MangaDexClient;
//!
//! # async fn run() -> anyhow::Result<()> {
//! let client = MangaDexClient::default();
//!
//! let manga_id = Uuid::new_v4();
//! let manga_res = client
//!     .manga()
//!     .get()
//!     .manga_id(&manga_id)
//!     .build()?
//!     .send()
//!     .await?;
//!
//! println!("manga view: {:?}", manga_res);
//! # Ok(())
//! # }
//! ```

use derive_builder::Builder;
use serde::Serialize;
use uuid::Uuid;

use crate::types::ReferenceExpansionResource;
use crate::v5::schema::MangaResponse;
use crate::HttpClientRef;

#[derive(Debug, Serialize, Clone, Builder)]
#[serde(rename_all = "camelCase")]
#[builder(setter(into, strip_option), pattern = "owned")]
pub struct GetManga<'a> {
    /// This should never be set manually as this is only for internal use.
    #[doc(hidden)]
    #[serde(skip)]
    #[builder(pattern = "immutable")]
    pub(crate) http_client: HttpClientRef,

    #[serde(skip)]
    pub manga_id: &'a Uuid,

    #[builder(setter(each = "include"), default)]
    pub includes: Vec<ReferenceExpansionResource>,
}

endpoint! {
    GET ("/manga/{}", manga_id),
    #[query] GetManga<'_>,
    #[flatten_result] MangaResponse
}

#[cfg(test)]
mod tests {
    #[cfg(feature = "chrono")]
    use chrono::{DateTime, Utc};
    #[cfg(not(feature = "time"))]
    use fake::faker::chrono::en::DateTime;
    #[cfg(not(feature = "time"))]
    use fake::Fake;
    use serde_json::json;
    #[cfg(feature = "time")]
    use time::OffsetDateTime;
    use url::Url;
    use uuid::Uuid;
    use wiremock::matchers::{method, path_regex};
    use wiremock::{Mock, MockServer, ResponseTemplate};

    use crate::types::{
        MangaDexDateTime, MangaRelation, ReferenceExpansionResource, RelationshipType,
    };
    use crate::v5::schema::RelatedAttributes;
    use crate::{HttpClient, MangaDexClient};

    #[tokio::test]
    async fn get_manga_fires_a_request_to_base_url() -> anyhow::Result<()> {
        let mock_server = MockServer::start().await;
        let http_client = HttpClient::builder()
            .base_url(Url::parse(&mock_server.uri())?)
            .build()?;
        let mangadex_client = MangaDexClient::new_with_http_client(http_client);

        let manga_id = Uuid::new_v4();

        let datetime = {
            #[cfg(not(any(feature = "chrono", feature = "time")))]
            let datetime: String = DateTime().fake();
            #[cfg(feature = "chrono")]
            let datetime: DateTime<Utc> = DateTime().fake();
            #[cfg(feature = "time")]
            let datetime = OffsetDateTime::now_utc();

            MangaDexDateTime::new(&datetime)
        };

        let response_body = json!({
            "result": "ok",
            "response": "entity",
            "data": {
                "id": manga_id,
                "type": "manga",
                "attributes": {
                    "title": {
                        "en": "Test Manga"
                    },
                    "altTitles": [],
                    "description": {},
                    "isLocked": false,
                    "links": {},
                    "originalLanguage": "ja",
                    "lastVolume": "1",
                    "lastChapter": "1",
                    "publicationDemographic": "shoujo",
                    "status": "completed",
                    "year": 2021,
                    "contentRating": "safe",
                    "chapterNumbersResetOnNewVolume": true,
                    "availableTranslatedLanguages": ["en"],
                    "tags": [],
                    "state": "published",
                    "version": 1,
                    "createdAt": datetime.to_string(),
                    "updatedAt": datetime.to_string(),
                },
                "relationships": [
                    {
                        "id": "a3219a4f-73c0-4213-8730-05985130539a",
                        "type": "manga",
                        "related": "side_story",
                    }
                ]
            }
        });

        Mock::given(method("GET"))
            .and(path_regex(r"/manga/[0-9a-fA-F-]+"))
            .respond_with(ResponseTemplate::new(200).set_body_json(response_body))
            .expect(1)
            .mount(&mock_server)
            .await;

        let res = mangadex_client
            .manga()
            .get()
            .manga_id(&manga_id)
            .build()?
            .send()
            .await?;

        assert_eq!(res.data.relationships[0].type_, RelationshipType::Manga);
        assert_eq!(
            res.data.relationships[0].related,
            Some(MangaRelation::SideStory)
        );
        assert!(res.data.relationships[0].attributes.is_none());

        Ok(())
    }

    #[tokio::test]
    async fn get_manga_handles_reference_expansion() -> anyhow::Result<()> {
        let mock_server = MockServer::start().await;
        let http_client = HttpClient::builder()
            .base_url(Url::parse(&mock_server.uri())?)
            .build()?;
        let mangadex_client = MangaDexClient::new_with_http_client(http_client);

        let manga_id = Uuid::new_v4();

        let datetime = {
            #[cfg(not(any(feature = "chrono", feature = "time")))]
            let datetime: String = DateTime().fake();
            #[cfg(feature = "chrono")]
            let datetime: DateTime<Utc> = DateTime().fake();
            #[cfg(feature = "time")]
            let datetime = OffsetDateTime::now_utc();

            MangaDexDateTime::new(&datetime)
        };

        let response_body = json!({
            "result": "ok",
            "response": "entity",
            "data": {
                "id": manga_id,
                "type": "manga",
                "attributes": {
                    "title": {
                        "en": "Test Manga"
                    },
                    "altTitles": [],
                    "description": {},
                    "isLocked": false,
                    "links": {},
                    "originalLanguage": "ja",
                    "lastVolume": "1",
                    "lastChapter": "1",
                    "publicationDemographic": "shoujo",
                    "status": "completed",
                    "year": 2021,
                    "contentRating": "safe",
                    "chapterNumbersResetOnNewVolume": true,
                    "availableTranslatedLanguages": ["en"],
                    "tags": [],
                    "state": "published",
                    "version": 1,
                    "createdAt": datetime.to_string(),
                    "updatedAt": datetime.to_string(),
                },
                "relationships": [
                    {
                        "id": "fc343004-569b-4750-aba0-05ab35efc17c",
                        "type": "author",
                        "attributes": {
                            "name": "Hologfx",
                            "imageUrl": null,
                            "biography": [],
                            "twitter": null,
                            "pixiv": null,
                            "melonBook": null,
                            "fanBox": null,
                            "booth": null,
                            "nicoVideo": null,
                            "skeb": null,
                            "fantia": null,
                            "tumblr": null,
                            "youtube": null,
                            "website": null,
                            "createdAt": "2021-04-19T21:59:45+00:00",
                            "updatedAt": "2021-04-19T21:59:45+00:00",
                            "version": 1
                        }
                    }
                ]
            }
        });

        Mock::given(method("GET"))
            .and(path_regex(r"/manga/[0-9a-fA-F-]+"))
            .respond_with(ResponseTemplate::new(200).set_body_json(response_body))
            .expect(1)
            .mount(&mock_server)
            .await;

        let res = mangadex_client
            .manga()
            .get()
            .manga_id(&manga_id)
            .include(&ReferenceExpansionResource::Author)
            .build()?
            .send()
            .await?;

        assert_eq!(res.data.relationships[0].type_, RelationshipType::Author);
        assert!(res.data.relationships[0].related.is_none());
        match res.data.relationships[0].attributes.as_ref().unwrap() {
            RelatedAttributes::Author(author) => assert_eq!(author.name, "Hologfx".to_string()),
            _ => panic!("Expected author RelatedAttributes"),
        }

        Ok(())
    }
}
