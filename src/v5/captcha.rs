//! CAPTCHA endpoint handler.
//!
//! <https://api.mangadex.org/docs.html#tag/Captcha>

mod solve;

use crate::v5::captcha::solve::SolveCaptchaBuilder;
use crate::HttpClientRef;

/// CAPTCHA endpoint handler builder.
#[derive(Debug)]
pub struct CaptchaBuilder {
    http_client: HttpClientRef,
}

impl CaptchaBuilder {
    #[doc(hidden)]
    pub(crate) fn new(http_client: HttpClientRef) -> Self {
        Self { http_client }
    }

    /// Solve a CAPTCHA challenge.
    ///
    /// <https://api.mangadex.org/docs.html#operation/post-captcha-solve>
    pub fn solve(&self) -> SolveCaptchaBuilder {
        SolveCaptchaBuilder::default().http_client(self.http_client.clone())
    }
}
