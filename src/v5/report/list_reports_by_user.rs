//! Builder for the report reasons list endpoint.
//!
//! <https://api.mangadex.org/docs.html#operation/get-report-reasons-by-category>
//!
//! # Examples
//!
//! ```rust
//! use mangadex_api::types::{ReportCategory, ReportStatus};
//! use mangadex_api::v5::MangaDexClient;
//!
//! # async fn run() -> anyhow::Result<()> {
//! let client = MangaDexClient::default();
//!
//! let res = client
//!     .report()
//!     .list_reports_by_user()
//!     .category(ReportCategory::Manga)
//!     .build()?
//!     .send()
//!     .await?;
//!
//! println!("reports: {:?}", res);
//! # Ok(())
//! # }
//! ```

use derive_builder::Builder;
use serde::Serialize;

use crate::types::{ReportCategory, ReportSortOrder, ReportStatus};
use crate::v5::schema::UserReportsListResponse;
use crate::HttpClientRef;

#[derive(Debug, Serialize, Clone, Builder, Default)]
#[serde(rename_all = "camelCase")]
#[builder(setter(into, strip_option), default, pattern = "owned")]
pub struct ListReportsByUser {
    #[doc(hidden)]
    #[serde(skip)]
    #[builder(pattern = "immutable")]
    pub(crate) http_client: HttpClientRef,

    pub limit: Option<u32>,
    pub offset: Option<u32>,
    pub category: Option<ReportCategory>,
    pub status: Option<ReportStatus>,
    pub order: Option<ReportSortOrder>,
}

endpoint! {
    GET "/report",
    #[query auth] ListReportsByUser,
    #[flatten_result] UserReportsListResponse
}

#[cfg(test)]
mod tests {
    #[cfg(feature = "chrono")]
    use chrono::{DateTime, Utc};
    #[cfg(not(feature = "time"))]
    use fake::faker::chrono::en::DateTime;
    #[cfg(not(feature = "time"))]
    use fake::Fake;
    use serde_json::json;
    #[cfg(feature = "time")]
    use time::OffsetDateTime;
    use url::Url;
    use uuid::Uuid;
    use wiremock::matchers::{method, path};
    use wiremock::{Mock, MockServer, ResponseTemplate};

    use crate::types::{MangaDexDateTime, ReportCategory, ReportStatus, ResponseType};
    use crate::v5::AuthTokens;
    use crate::{HttpClient, MangaDexClient};

    #[tokio::test]
    async fn list_reports_by_user_fires_a_request_to_base_url() -> anyhow::Result<()> {
        let mock_server = MockServer::start().await;
        let http_client = HttpClient::builder()
            .base_url(Url::parse(&mock_server.uri())?)
            .auth_tokens(AuthTokens {
                session: "sessiontoken".to_string(),
                refresh: "refreshtoken".to_string(),
            })
            .build()?;
        let mangadex_client = MangaDexClient::new_with_http_client(http_client);

        let report_id = Uuid::new_v4();
        let datetime = {
            #[cfg(not(any(feature = "chrono", feature = "time")))]
            let datetime: String = DateTime().fake();
            #[cfg(feature = "chrono")]
            let datetime: DateTime<Utc> = DateTime().fake();
            #[cfg(feature = "time")]
            let datetime = OffsetDateTime::now_utc();

            MangaDexDateTime::new(&datetime)
        };

        let response_body = json!({
            "result": "ok",
            "response": "collection",
            "data": [
                {
                    "id": report_id,
                    "type": "report",
                    "attributes": {
                        "details": "The manga was a troll submission.",
                        "objectId": "2",
                        "status": "accepted",
                        "createdAt": datetime.to_string()
                    },
                    "relationships": []
                }
            ],
            "limit": 10,
            "offset": 0,
            "total": 1
        });

        Mock::given(method("GET"))
            .and(path("/report"))
            .respond_with(ResponseTemplate::new(200).set_body_json(response_body))
            .expect(1)
            .mount(&mock_server)
            .await;

        let res = mangadex_client
            .report()
            .list_reports_by_user()
            .category(ReportCategory::Manga)
            .build()?
            .send()
            .await?;

        assert_eq!(res.response, ResponseType::Collection);
        assert_eq!(res.limit, 10);
        assert_eq!(res.offset, 0);
        assert_eq!(res.total, 1);
        let report = &res.data[0];
        assert_eq!(report.id, report_id);

        assert_eq!(
            report.attributes.details,
            "The manga was a troll submission.".to_string()
        );
        assert_eq!(report.attributes.object_id, "2".to_string());
        assert_eq!(report.attributes.status, ReportStatus::Accepted);

        Ok(())
    }
}
