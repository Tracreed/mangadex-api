use serde::{Deserialize, Serialize};

pub use md_datetime::MangaDexDateTime;

#[cfg(not(any(feature = "chrono", feature = "time")))]
mod md_datetime {
    use super::*;

    /// Newtype struct for handling datetime fields in MangaDex.
    #[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
    pub struct MangaDexDateTime(String);

    impl MangaDexDateTime {
        pub fn new<S: Into<String>>(datetime: S) -> Self {
            Self(datetime.into())
        }
    }

    impl From<&str> for MangaDexDateTime {
        fn from(datetime: &str) -> Self {
            Self(datetime.into())
        }
    }

    impl From<String> for MangaDexDateTime {
        fn from(datetime: String) -> Self {
            Self(datetime)
        }
    }

    impl AsRef<str> for MangaDexDateTime {
        fn as_ref(&self) -> &str {
            &self.0
        }
    }

    impl std::fmt::Display for MangaDexDateTime {
        fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
            fmt.write_str(self.as_ref())
        }
    }
}

#[cfg(feature = "chrono")]
mod md_datetime {
    use chrono::{DateTime, Utc};
    use serde::Serializer;

    use super::*;
    use crate::{MANGADEX_DATETIME_DE_FORMAT, MANGADEX_DATETIME_SER_FORMAT};

    /// Newtype struct for handling datetime fields in MangaDex.
    #[derive(Debug, Clone, Deserialize, PartialEq)]
    pub struct MangaDexDateTime(DateTime<Utc>);

    impl MangaDexDateTime {
        pub fn new(datetime: &DateTime<Utc>) -> Self {
            Self(*datetime)
        }
    }

    impl From<DateTime<Utc>> for MangaDexDateTime {
        fn from(datetime: DateTime<Utc>) -> Self {
            Self(datetime)
        }
    }

    impl AsRef<DateTime<Utc>> for MangaDexDateTime {
        fn as_ref(&self) -> &DateTime<Utc> {
            &self.0
        }
    }

    impl Serialize for MangaDexDateTime {
        fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            serializer.serialize_str(
                &self
                    .as_ref()
                    .format(MANGADEX_DATETIME_SER_FORMAT)
                    .to_string(),
            )
        }
    }

    impl std::fmt::Display for MangaDexDateTime {
        fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
            fmt.write_str(
                &self
                    .as_ref()
                    .format(MANGADEX_DATETIME_DE_FORMAT)
                    .to_string(),
            )
        }
    }
}

#[cfg(feature = "time")]
mod md_datetime {
    use serde::{Deserializer, Serializer};
    use time::{format_description, OffsetDateTime};

    use super::*;
    use crate::{MANGADEX_DATETIME_DE_FORMAT, MANGADEX_DATETIME_SER_FORMAT};

    /// Newtype struct for handling datetime fields in MangaDex.
    #[derive(Debug, Clone, PartialEq)]
    pub struct MangaDexDateTime(OffsetDateTime);

    impl MangaDexDateTime {
        pub fn new(datetime: &OffsetDateTime) -> Self {
            Self(*datetime)
        }
    }

    impl From<OffsetDateTime> for MangaDexDateTime {
        fn from(datetime: OffsetDateTime) -> Self {
            Self(datetime)
        }
    }

    impl AsRef<OffsetDateTime> for MangaDexDateTime {
        fn as_ref(&self) -> &OffsetDateTime {
            &self.0
        }
    }

    impl Serialize for MangaDexDateTime {
        fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            let format = format_description::parse(MANGADEX_DATETIME_SER_FORMAT).unwrap();

            serializer.serialize_str(&self.as_ref().format(&format).unwrap())
        }
    }

    impl<'de> Deserialize<'de> for MangaDexDateTime {
        fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where
            D: Deserializer<'de>,
        {
            let s: String = Deserialize::deserialize(deserializer)?;

            let format = format_description::parse(MANGADEX_DATETIME_DE_FORMAT).unwrap();

            let datetime = OffsetDateTime::parse(&s, &format).unwrap();

            Ok(MangaDexDateTime(datetime))
        }
    }

    impl std::fmt::Display for MangaDexDateTime {
        fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
            let format = format_description::parse(MANGADEX_DATETIME_DE_FORMAT).unwrap();

            fmt.write_str(&self.as_ref().format(&format).unwrap())
        }
    }
}
